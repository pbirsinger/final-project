var capitalize = function(s) {
    return s.charAt(0).toUpperCase() + s.slice(1);
}

var width = 960,
    height = 500,
    color = d3.scale.category20c();

var treemap = d3.layout.treemap()
    .size([width, height])
    .sticky(true)
    .value(function(d) { return d.total; });

var div = d3.select("#chart").append("div")
    .style("position", "relative")
    .style("width", width + "px")
    .style("height", height + "px");

var qvData = [];
var qvLabels = ['name', 'category', 'total', 'unique'];
var qvCapLabels = qvLabels.map(capitalize);
var qvAllData = [qvData, qvLabels, qvCapLabels];

d3.json("web.json", function(json) {
  div.data([json]).selectAll("div")
      .data(treemap.nodes)
    .enter().append("div")
      .attr("class", "cell")
      .call(dataAttrs)
      .style("background", function(d) { return d.children ? color(d.name) : null; })
      .call(cell)
      .text(function(d) { return d.children ? null : d.name; });

  d3.select("#size").on("click", function() {
    div.selectAll("div")
        .data(treemap.value(function(d) { return d.total; }))
      .transition()
        .duration(1500)
        .call(cell);

    d3.select("#size").classed("active", true);
    d3.select("#count").classed("active", false);
  });
  
  d3.select("#count").on("click", function() {
    div.selectAll("div")
        .data(treemap.value(function(d) { return d.unique; }))
      .transition()
        .duration(1500)
        .call(cell);

    d3.select("#size").classed("active", false);
    d3.select("#count").classed("active", true);
  });

  // d3.select("#count").on("click", function() {
  //   div.selectAll("div")
  //       .data(treemap.value(function(d) { return 1; }))
  //     .transition()
  //       .duration(1500)
  //       .call(cell);
  // 
  //   d3.select("#size").classed("active", false);
  //   d3.select("#count").classed("active", true);
  // });
});

function dataAttrs() {
    // Only assign data attributes to the small cells, not the
    // bigger category cells
    this
        .attr("data-name", function(d) {
            if (d.unique === undefined) return undefined;
            return d.name; })
        .attr("data-category", function(d) { 
            if (d.unique === undefined) return undefined;
            return d.category; })
        .attr("data-unique", function(d) { return d.unique; })
        .attr("data-total", function(d) { return d.total; })
        .each(function(d) {
            if (d.unique !== undefined) {
                qvData.push([d.name, d.category, 
                             parseInt(d.total), parseInt(d.unique)]);
            }
        });
}

function cell() {
  this
      .style("left", function(d) { return d.x + "px"; })
      .style("top", function(d) { return d.y + "px"; })
      .style("width", function(d) { return Math.max(0, d.dx - 1) + "px"; })
      .style("height", function(d) { return Math.max(0, d.dy - 1) + "px"; });
}