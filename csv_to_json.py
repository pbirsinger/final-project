import csv
import json

f = open( 'recent_earthquakes.csv', 'r' )
reader = csv.DictReader( f, fieldnames = ( "Src","Eqid","Version","Datetime","Lat","Lon","Magnitude","Depth","NST","Region" ) )
out = json.dumps( [ row for row in reader ] )
m = open('recent_earthquakes.json','w')
m.write(out)
m.close()
