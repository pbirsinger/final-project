
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

var QueryVis = {
    
    count: {'c': 0},
    
    Q: function(dataset, premade_selection) { 
        this.facts = "";
        this.infer_facts = "";
        this.rules = ""; 
        this.query_string = "";
        this.class = "";
        
        this.main_data = dataset;
        
        this.set_data_vars = function(dataset) {
            if (dataset instanceof Array && dataset.length === 3){
                // if the json has already been processed
                this.cols = dataset[1];
                this.rows = dataset[0];
                this.var_cols = dataset[2];
            } else if (typeof(dataset) == "string" && dataset.indexOf(".json") != -1){
                // if you are served a file
                // TODO: test this case
                $.ajax({
            		url: dataset + "#e" + Math.random() ,
            		async: false,
            		success: function(data) {
            			console.log(data);
            			j = data;
            		},
            		dataType: 'json'
            	});
            	
            	var dataset1 = QVUtil.process_json(j); 
                dataset = dataset1;
                this.cols = dataset[1];
                this.rows = dataset[0];
                this.var_cols = dataset[2];
            
            } else {
                // else assume they entered a string json
                var dataset1 = QVUtil.process_json(dataset); 
                dataset = dataset1;
                this.cols = dataset[1];
                this.rows = dataset[0];
                this.var_cols = dataset[2];
            }
        };
        
        this.set_data_vars(dataset);
        this.infer_facts = QVUtil.make_facts(this.rows);
        
        if (!premade_selection) {
            this.selection = QVUtil.init_divs([this.rows, this.cols, this.var_cols]);
        } else {
             this.selection = premade_selection;
        }
        
        this.class = $(this.selection).parent().attr("class");

        this.addFact = function(fact){
            if (this.facts === "") {
                this.facts += fact;
            } else { 
                if (this.rules.indexOf(fact) == -1){
                    this.facts += '\n' + fact;
                }
            }
        };
        
        this.addRule = function(rule){
            if (this.rules === "") {
                this.rules += rule;
            } else {
                if (this.rules.indexOf(rule)== -1){ 
                    this.rules += '\n' + rule;
                }
            }  
        };
        
        this.update = function(dataset, option){
            // remove option will just remove queries that do not match from the 
            var old_ids = this.selected_ids; 
            
            if (dataset) { 
                this.infer_facts = "";
                this.set_data_vars(dataset);
                this.infer_facts = QVUtil.make_facts(this.rows);
            
            } else {
                // if you are not given a dataset, you need to reload the file
                // and get the data from that
                // TODO: test this case 
                dataset = this.main_data;
                this.infer_facts = "";
                this.set_data_vars(dataset);
                this.infer_facts = QVUtil.make_facts(this.rows);
            }
            
            var equal_ids = function(id1,id2) {
                // check first if undefined
                if (!id1 && !id2) {return 1;}
                if ((!id1 && id2) || (!id2 && id1)) { return 0; }
                
                if (id1.length != id2.length) {return 0; }
                for (i = 0; i < id1.length; i++) {
                    if (id1[i] != id2[i]) {return 0;}
                }
                return 1;
            }
            
            var updated_selection;
            if (this.query_string) {
                updated_selection = this.query(this.query_string);
                var new_ids = this.selected_ids;
            } else {
                updated_selection = this.selection;
            }
            if (!equal_ids(new_ids,old_ids)) { 
                return updated_selection; } 
            else {return 0; }
            
        };
        
        this.query = function(q){
            $.ajax({
                    url: "datalog.js",
                    dataType: "script",
                    async: false,           // <-- this is the key
                    success: function () {
                        // all good...
                    },
                    error: function () {
                        throw new Error("Could not load script " + script);
                    }
                });
            
            // Must get rid of the old assignments when you
            // make multiple queries
            this.assignments = "";
            
            this.query_string = q;
            input_str = "";
            if(this.facts){ input_str += this.facts + '\n'; }
            if (this.rules) { input_str += this.rules + '\n'; }
            if (this.infer_facts) {input_str += this.infer_facts; } 
            input_str += '\n';
            
            // this.facts + '\n' + this.rules + '\n' + this.infer_facts;
            
            // Fix for when facts and rules are empty 
            // If this is not included, datalog will infinite loop in this case
            if (input_str == "" || input_str == "\n"){
                var f = (q.split('('))[0]; 
                var num_var = (q.split(',')).length;
                var var_str = "";
                var alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                
                // What do you do if there are over 26 k? No one knows.
                for (var k = 0; k < num_var; k++){
                    if (k == 0){
                        var_str += alpha[k];
                    } else {
                        var_str += ',' + alpha[k]; 
                    }
                    
                }
                input_str += f + '(' + var_str + ').';
            }
            console.log(input_str);
            console.log(this.query_string);
            
            var freeform_output = freeform(input_str, this.query_string);
            console.log(freeform_output);
            
            this.selected_ids = QVUtil.process_freeform_output(freeform_output);
            console.log(this.selected_ids);
            var query_selectionJQ = QVUtil.select_by_ids(this);
            
            var query_selectionD3 = d3.selectAll(query_selectionJQ);
            return query_selectionD3;
        };
    } 
};

var QVUtil = {
    
  make_facts: function(rows) {
      var facts = "";
      var sanitize = function(input){
          if (typeof(input)=='string') {
        	input = input.replace(/[\s~!@#$%^&*()-=_]/g,"");
        	input = input.toLowerCase();
        	} 
        	return input;
        };
      
      var addfact = function(fact){
          if (facts === "") {
              facts += fact;
          } else { 
              facts += '\n' + fact;
          }
      };
      
      for (var i = 0, u; u = rows[i++]; ){
         console.log(u);
         var current_fact = "data(";
         for (var m = 0; m < u.length; m++ ){
             if (current_fact == "data(") {
                 current_fact += sanitize(u[m]);   /// HEREEEEE
             } else {
                 current_fact += "," + sanitize(u[m]);  ///HEREEEEE
             }
         }
         current_fact += ").";
         addfact(current_fact);
      }
      return facts;
  },
    
  process_freeform_output: function(free_out){
   		//var free_out = free_out.replace(/\s/g,"");
  		var index = free_out.indexOf('Id =');
  		var next,id;
  		var id_arr = [];
  		while (index != -1){
  			index = index+5;
  			next = free_out.indexOf('\n',index+1);
  			id = free_out.substring(index, next);
  			id_arr.push(id)
  			index = free_out.indexOf('Id =',index)
  		}	
            console.log(id_arr);
            
    		return id_arr;
    },

  
  init_divs: function(dataset){
        var imp = dataset[2];
        
        // increase count for unique class
        QueryVis.count['c'] = QueryVis.count['c'] + 1; 
        var cnt = QueryVis.count['c'];
        
        // attach chart to body -- TODO: make optional "div" / "body" arguments
        var chart = d3.select("body").append("div")
                    .attr("class", "chart" + String(cnt));

        var p = chart.selectAll("div")
                    .data(dataset[0])
                    .enter().append("div"); 
                       
        for (var j = 0; j < imp.length; j ++ ){
            // attach data attributes to nodes
            p.attr("data-"+imp[j], function(d){ return d[j];  });
        }
        return p;
  },   
  
  // removes all items from 
  remove_divs: function(QVobj){
    assignments = QVobj.assignments;
    var_cols = QVobj.var_cols;
    cols = QVobj.cols;
    for (var j = 0, asgn; asgn = assignments[j++]; ){      
          ind = var_cols.indexOf(asgn[0]);
          attribute = "data-"+cols[ind];
          val = asgn[1];
          
          val = asgn[1]
          if (val != "Free") {
              s = QVobj.selection[0];
              for (var i = 0, ob; ob = s[i++]; ){
                  if ($(ob).attr(attribute) != val) {
                      $(ob).hide();
                  }
              }
          }
      }
      
  },
  
  // Returns selection matching assignments in query
  // TODO: remove only those of the class of the given object
  // This is located in QVobj.class
  select_asgn: function(QVobj){
      assignments = QVobj.assignments;
      var_cols = QVobj.var_cols;
      cols = QVobj.cols;
      var total_selection = 0;
      for (var j = 0, asgn; asgn = assignments[j++]; ){      
            ind = var_cols.indexOf(asgn[0]);
            attribute = "data-"+cols[ind];
            val = asgn[1]; 
            
            if (val != "Free") {
                console.log("got here");
                
                if (total_selection == 0) {
                    total_selection =  $('*['+ attribute +'="'+ val +'"]');
                } else {

                    new_selection = $('*['+ attribute +'="'+ val +'"]');
                    total_selection.add(new_selection);
                }
                // s = QVobj.selection[0];
                //                 for (var i = 0, ob; ob = s[i++]; ){
                //                     if ($(ob).attr(attribute) != val) {
                //                         $(ob).hide();
                //                     }
                //                 }
            }
        }
        return total_selection; 
        
  },
  
  select_by_ids: function(QVobj){
        assignments = QVobj.selected_ids;
        console.log(assignments);
        
        var total_selection = 0;
        for (var j = 0, asgn; asgn = assignments[j++]; ){      
              console.log(asgn);
              attribute = "data-id";
              val = asgn; 
              if (total_selection == 0) {
                    total_selection =  $('*['+ attribute +'="'+ val +'"]');
                } else {
                    
                    new_selection = $('*['+ attribute +'="'+ val +'"]');
                    total_selection = total_selection.add(new_selection);
                }
         }
         
         
         return total_selection; 
    },
  
       
  // Input: json object
  // Output: [ data points, column values ]
  process_json: function(json){
      // json entered in form [
      //      {id:'0', username:'mike',email:'mike@mikesplace.com'},
      //      {id:'1', username:'jane',email:'jane@bigcompany.com'},
      //      {id:'2', username:'stan',email:'stan@stanford.com'}
      // ]
      // json = $.parseJSON(json)
      var getKeys = function(p){
         var keys = [];
         for (var key in p) {
           if (p.hasOwnProperty(key)) {
             keys.push(key);
           }
         }
         return keys;
      }
      var data_points = [];
      var j_len = json.length;
      var j = 0;
      var current_keys = getKeys(json[0]);
      var var_keys = [];
      for (var i = 0, k; k = current_keys[i++]; ) {
          var_keys.push(k.capitalize());
      }
      console.log(current_keys);
      
      for (var i = 0; i < j_len; i++ ){
        data_points[i] = [];
        var dict = json[i];
        for (var j = 0; j < current_keys.length; j ++ ){
            var new_data = dict[String(current_keys[j])];
            if (!isNaN(new_data)){ new_data = +new_data; }
            data_points[i].push(new_data);
        }
      }
      return [data_points, current_keys, var_keys];
  },  
  
  isDigit: function(aChar) {
        myCharCode = aChar.charCodeAt(0);
        if((myCharCode > 47) && (myCharCode <  58))
        {
           return 1;
        }
        return 0;
  },
    
};


// item = new QueryVis.Q();
// 
// item.addFact('data(a,b,c).');
// item.addRule('cooldata(X,Y) :- data(X,a,b), data(Y,a,b).');
// item.query('data(X,Y,Z)');
// console.log(item.assignments);

// item.addFact(['data',['X','Y','Z']])
//item.addRule(['cooldata',['X','Y']], [ ['data',['X','b','c']],['data',['Y','b','d']] ])

